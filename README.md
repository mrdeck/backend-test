# Description

Your assignment is to implement the functions described in the test suite. Write as many of them as you can, but aim for completing one before moving on to the next.

The functions should not mutate the input object.

- `path` should retrieve a value at a given path in the source object
- `update` should make a shallow clone of an object, setting or overriding the nodes required to create the given path, and placing the specific value at the tail end of that path
- `flatten` should return a new list by pulling every item out of it (and all its sub-arrays) and putting them in a new array, depth-first

You are not allowed to copy or look at solutions from other libraries. Good Luck!

import { path } from './path'

describe('path', () => {
  const deepObject = {
    a: { b: { c: 'c' } },
    falseVal: false,
    nullVal: null,
    undefinedVal: undefined,
    arrayVal: ['arr'],
  }

  it('should take a path and an object and return the value at the path, or undefined', () => {
    const obj = {
      a: {
        b: {
          c: 100,
          d: 200,
        },
        e: {
          f: [100, 101, 102],
          g: 'G',
        },
        h: 'H',
      },
      i: 'I',
      j: ['J'],
    }

    expect(path(['a', 'b', 'c'], obj)).toBe(100)
    expect(path([], obj)).toEqual(obj)
    expect(path(['a', 'e', 'f', 1], obj)).toBe(101)
    expect(path(['j', 0], obj)).toBe('J')
    expect(path(['j', 1], obj)).toBe(undefined)
  })

  it("should get a deep property's value from objects", () => {
    expect(path(['a', 'b', 'c'], deepObject)).toBe('c')
    expect(path(['a'], deepObject)).toEqual(deepObject.a)
  })

  it('should return undefined for items not found', () => {
    expect(path(['a', 'b', 'foo'], deepObject)).toBe(undefined)
    expect(path(['bar'], deepObject)).toBe(undefined)
    expect(path(['a', 'b'], { a: null })).toBe(undefined)
  })

  it('should work with falsy values', () => {
    // eslint-disable-next-line @typescript-eslint/unbound-method
    expect(path(['toString'], false)).toEqual(Boolean.prototype.toString)
  })
})

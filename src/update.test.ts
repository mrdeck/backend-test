import { update } from './update'

describe('update', function () {
  it('should make a shallow clone of an object, overriding only what is necessary for the path', () => {
    const obj1 = {
      a: { b: 1, c: 2, d: { e: 3 } },
      f: { g: { h: 4, i: [5, 6, 7], j: { k: 6, l: 7 } } },
      m: 8,
    }

    const obj2 = update(['f', 'g', 'i', 1], 42, obj1)

    expect(obj2.f.g.i).toEqual([5, 42, 7])

    expect(obj2.a).toEqual(obj1.a)
    expect(obj2.m).toEqual(obj1.m)
    expect(obj2.f.g.h).toEqual(obj1.f.g.h)
    expect(obj2.f.g.j).toEqual(obj1.f.g.j)
  })

  it('should be the equivalent of the object spread if the property is not on the original object', function () {
    const obj1 = { a: 1, b: { c: 2, d: 3 }, e: 4, f: 5 }
    const obj2 = update(['x', 0, 'y'], 42, obj1)

    expect(obj2).toEqual({ a: 1, b: { c: 2, d: 3 }, e: 4, f: 5, x: [{ y: 42 }] })
    expect(obj2.a).toEqual(obj1.a)
    expect(obj2.b).toEqual(obj1.b)
    expect(obj2.e).toEqual(obj1.e)
    expect(obj2.f).toEqual(obj1.f)
  })

  it('should replace the source object when the path is empty', function () {
    expect(update([], 3, { a: 1, b: 2 })).toBe(3)
  })

  it('should replace `undefined` with a new object', function () {
    expect(update(['foo', 'bar', 'baz'], 42, { foo: undefined })).toEqual({
      foo: { bar: { baz: 42 } },
    })
  })

  it('should replace `null` with a new object', function () {
    expect(update(['foo', 'bar', 'baz'], 42, { foo: null })).toEqual({
      foo: { bar: { baz: 42 } },
    })
  })
})

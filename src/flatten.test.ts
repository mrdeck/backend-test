import { flatten } from './flatten'

describe('flatten', () => {
  it('should turn a nested list into a flat list', () => {
    const testValue1 = [1, [2], [3, [4, 5], 6, [[[7], 8]]], 9, 10]
    const testValue2 = [[[[3]], 2, 1], 0, [[-1, -2], -3]]
    const testValue3 = [1, 2, 3, 4, 5]

    expect(flatten(testValue1)).toEqual([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])
    expect(flatten(testValue2)).toEqual([3, 2, 1, 0, -1, -2, -3])
    expect(flatten(testValue3)).toEqual([1, 2, 3, 4, 5])
  })

  it('should not be destructive', () => {
    const testValue = [1, [2], [3, [4, 5], 6, [[[7], 8]]], 9, 10]

    expect(flatten(testValue)).not.toBe(testValue)
  })

  it('should handle array-like objects', () => {
    const testValue = { length: 3, 0: [1, 2, [3]], 1: [], 2: ['a', 'b', 'c', ['d', 'e']] }

    expect(flatten(testValue)).toEqual([1, 2, 3, 'a', 'b', 'c', 'd', 'e'])
  })

  it('should flatten an array of empty arrays', () => {
    const testValue1 = [[], [], []]

    expect(flatten(testValue1)).toEqual([])
  })
})

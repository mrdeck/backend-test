module.exports = {
  extends: './eslint/node',
  parserOptions: {
    project: require.resolve('./tsconfig.json'),
  },
}
